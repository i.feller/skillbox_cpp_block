#include <cstdio>
#include <iostream>
using namespace std;

class Animal
{
    
public:
    virtual ~Animal() = default;

    virtual void Voice()
    {
        cout << "Animals say: " << endl;
    }
};

class Dog : public Animal
{
public:
    void Voice() override
    {
        Animal::Voice();
        cout << "Woof!" << endl;
    }
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        Animal::Voice();
        cout << "Meow!" << endl;
    }
};

class Bird : public Animal
{
public:
    void Voice() override
    {
        Animal::Voice();
        cout << "Tweet!" << endl;
    }
};

unsigned main()
{
    Animal* p[3];
    p[0] = new Cat;
    p[1] = new Dog;
    p[2] = new Bird;

    for (auto o : p)
    {
        o->Voice();
        delete o;
    }
}
