﻿#include <algorithm>
#include <cassert>
#include <iostream>
#include "Helpers.h"
#include <time.h>
#include <string>
#include <ctime>
#include <chrono>
#include <thread>
#include <iomanip>
#include <stack>
#include <iostream>
#include <xutility>
#pragma warning(disable : 4996) //_CRT_SECURE_NO_WARNINGS

using namespace std;
const int N = 11;

class example
{
    //border length
    void border(int x)
    {
        for (auto i = 0; i < x; i++)
        {
            cout << '*';
            if (i == trunc(x / 2))
            {
                cout << "get some random nums*";
            }
        }
        cout << endl;
    }

    static void print_hello()
    {
        cout << "Hello World!" << endl;
    }


    /*
    Скачать и установить Visual Studio, ознакомиться с интерфейсом и повторить действия спикера.
    */
public:
    void lesson11()
    {
        auto x = 100;
        auto y = x + 100;
        auto b = 0;
        float test = 100500;
        float test2 = 25;

        auto mult = test / test2;

        b = +2;

        //set random generator based on current time
        srand(static_cast<unsigned>(time(nullptr)));

        print_hello();

        cout << mult << endl;

        //print some random nums

        border(21);
        for (auto i = 0; i < 10; i++)
        {
            cout << rand() % 50 << endl;
        }
        border(47);
    }

    /*
    Инициализировать переменную типа std::string любым значением.
    Вывести саму строковую переменную, вывести длину строки, вывести первый и последний символы этой строки. Для вывода использовать std::cout <<
    */
    static void lesson14()
    {
        string word{"Случайность не случайна."};

        cout << word << endl;

        cout << word.length() << endl;

        cout << word.at(0) << endl;

        cout << word.at(word.length() - 1) << endl;
    }

    /*
    Используя цикл и условия вывести в консоль все четные числа от 0 до N (N задавать константой в начале программы).
    Написать функцию, которая в зависимости от своих параметров печатает в консоль либо четные, либо нечетные числа от 0 до N (N тоже сделать параметром функции).
    Минимизировать количество циклов и условий.
    */
    static void lesson15(int num = 0)
    {
        while (num < N)
        {
            cout << num << endl;
            num += 2;
        }
    }

    /*
    Cоздать двумерный массив размерности NxN и заполнить его таким образом, чтобы элемент с индексами i и j был равен i + j.
    Вывести этот массив в консоль.
    Вывести сумму элементов в строке массива, индекс которой равен остатку деления текущего числа календаря на N. (в двумерном массиве a[i][j], i — индекс строки).
    */
    static void lesson16()
    {
        // current date/time based on current system
        auto now = time(nullptr);
        const auto ltm = localtime(&now);

        int array[N][N];

        for (auto i = 0; i < N; i++)
        {
            for (auto j = 0; j < N; j++)
            {
                array[i][j] = i + j;
            }
        }

        //print array as table
        cout << "array: " << N << "x" << N << endl;
        for (auto i = 0; i < N; i++)
        {
            for (auto j = 0; j < N; j++)
            {
                if (i + j < 10)
                    cout << " ";
                cout << array[i][j] << " ";
            }
            cout << endl;
        }
        cout << endl;

        //get sum of numbers from string index based on rest of dividition of current date and N
        static int number;
        for (auto i = 0; i < N; i++)
        {
            if (i == ltm->tm_mday % N)
                for (auto j = 0; j < N; j++)
                {
                    number += array[i][j];
                }

            if (ltm->tm_mday % N == i)
                cout << "string: " << i << " number: " << number << " ";
        }
    }
};

class vector
{
private:
    double x = 10;
    double y = 10;
    double z = 10;

public:
    void show()
    {
        cout << endl << x << ' ' << y << ' ' << z << endl;
    }

    float lesson17(float a, float b, float c)
    {
        return sqrt(pow(a, 2) + pow(b, 2) + pow(c, 2));
    }
};



/*
lesson18
*/
class sstack
{
    int* numers;
    unsigned last_index;
    unsigned size = 6;

public:
    sstack()
    {
        last_index = 0;
        numers = new int[size];
    }

    ~sstack()
    {
        delete[] numers;
    }

     explicit sstack(unsigned s)
    {
        size = s;
        last_index = 0;
        numers = new int[size];
    }

    void push(unsigned new_element)
    {
        if (last_index >= size)
        {
            const auto tempArray = new int[size * 2];
            for (auto i = 0u; i < size; i++)
            {
                tempArray[i] = numers[i];
            }
            delete[] numers;
            numers = tempArray;
            size *= 2;
        }
        numers[last_index++] = new_element;
    }

    auto pop()
    {
        assert(last_index > 0);
        last_index--;
        return numers[last_index];
    }
};

/*
* Get input and validate to be sure it's a digit
*/
int typeAndValNum()
{
    auto valid = false;
    int num;
    do
    {
        cin >> num;
        if (cin.good())
        {
            valid = true;
        }
        else
        {
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            cout << "Вы ввели не число. Пожалуйста введите число." << endl;
        }
    }
    while (!valid);
    return num;
}

void lesson18()
{
    unsigned size = 0;
    do
    {
        cout << "Сколько чисел вы хотите ввести?" << endl;
        size = typeAndValNum();
          if (size <= 1)
          {
              cout << "Пожалуйста введите число больше единицы" << endl;
          }
    } while (size <= 1);
    sstack stack_object(size);

    cout << "А теперь сами числа:" << endl;

    for (unsigned i = 0; i < size; i++)
    {
        stack_object.push(typeAndValNum());
    }

    cout << "Давайте выведим верхний элемент: " << stack_object.pop() << endl;
    cout << "Вот что осталось:" << endl;
    for (unsigned i = 1; i < size; i++)
    {
        cout << stack_object.pop() << endl;
    }
}

/*
Main function
Doing stuff
*/
int main()
{
    setlocale(LC_ALL, "rus");
    // example ex;
    // vector v;
    // ex.lesson11();
    // cout << ex.lesson13(5, 6);
    // ex.lesson14();
    // ex.lesson15(1);
    // ex.lesson16();
    // v.show();
    // cout << v.lesson17(2,2,2) << endl;
    lesson18();
}
